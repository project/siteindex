
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Description
 * Installation
 * Configuration
 * Reporting
 * Limitations
 
 
INTRODUCTION
------------

Author and current maintainer: James Marks <jmarks@mercycorps.org>
Initial concept and feedback: Eura Ryan <e.g.ryan@gmail.com>

Site Index was written, and is currently supported, for Drupal 6. Support for
Drupal 7 is planned but not yet scheduled. Support for Drupal <= 5 is not
planned.


DESCRIPTION
-----------

Site Index provides the ability to create an alphabetically-ordered index of
site content similar in style to a typical book content index. 

Index entries can be created for either site content or for redirection to other
index entries.

Site index entries can be created either from the admin/build/siteindex page or
from the node edit form through a site entry fieldset added to node edit form if
so configured in the siteindex settings.


INSTALLATION
------------

Install as usual, see http://drupal.org/node/70151 for further information.


CONFIGURATION
-------------

Options include:
- Setting index page title and path
- Showing notices on node edit forms for nodes that are not included in the site
  index
- Including index entry fields for adding or deleting index entries in the node
  edit form for specified content types
- Periodic auto-verification of index link functionality with exclusion of
  non-functional links


REPORTING
---------

Report of non-functional index entry links at admin/reports/siteindex.


LIMITATIONS
-----------

Index entries can be created for internal site content or for other index
entries only. Links to external content will not pass validation.
